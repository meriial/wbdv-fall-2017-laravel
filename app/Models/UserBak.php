<?php

namespace App\Models;

class UserBak {
    public $name;
    public $handle;
    public $tweets;
    public $tweetCount = '36.2K';
    public $following = '45';
    public $followers = '41.5M';
    public $likes = 19;
    public $moments = 5;
    public $imgUrl = 'https://pbs.twimg.com/profile_banners/25073877/1506284043/1500x500';
    public $images = [
        'https://pbs.twimg.com/media/DBVU66xWAAA74m_.jpg:thumb',
        'https://pbs.twimg.com/media/DBVU66xWAAA74m_.jpg:thumb',
        'https://pbs.twimg.com/media/DBVU66xWAAA74m_.jpg:thumb',
        'https://pbs.twimg.com/media/DBVU66xWAAA74m_.jpg:thumb',
        'https://pbs.twimg.com/media/DBVU66xWAAA74m_.jpg:thumb',
        'https://pbs.twimg.com/media/DBVU66xWAAA74m_.jpg:thumb'
    ];
}
