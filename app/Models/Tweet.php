<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tweet extends Model {
    // public $user;
    // // public $content = '';
    public $time = '18h';
    public $retweets = '55K';
    public $likes = '96K';

    // public function __construct()
    // {
        // $this->user = $user;
        // $this->content = $content;
    // }
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function comments()
    {
        return rand(10, 50) . 'K';
    }
}
