<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    public $images = [
        'https://pbs.twimg.com/media/DBVU66xWAAA74m_.jpg:thumb',
        'https://pbs.twimg.com/media/DBVU66xWAAA74m_.jpg:thumb',
        'https://pbs.twimg.com/media/DBVU66xWAAA74m_.jpg:thumb',
        'https://pbs.twimg.com/media/DBVU66xWAAA74m_.jpg:thumb',
        'https://pbs.twimg.com/media/DBVU66xWAAA74m_.jpg:thumb',
        'https://pbs.twimg.com/media/DBVU66xWAAA74m_.jpg:thumb'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'handle'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
