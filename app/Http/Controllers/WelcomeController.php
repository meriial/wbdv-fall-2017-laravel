<?php

namespace App\Http\Controllers;

use Faker\Factory;
use App\Models\User;
use App\Models\Tweet;

// app/Http/Controllers/WelcomeController.php


class WelcomeController extends Controller {

    public function index() {
        $faker = Factory::create();

        $user = User::find(1);
        // $user = new User();
        // $user->name = $faker->name;
        // $user->handle = $faker->word;

        // $tweet1 = new Tweet($user, $faker->sentence);
        // $tweet2 = new Tweet($user, $faker->sentence);
        // $tweet3 = new Tweet($user, $faker->sentence);

        $tweetFeed = Tweet::orderBy('id', 'desc')->get();
        // foreach ($tweetFeed as $tweet) {
        //     $tweet->user = $user;
        // }

        // $tweetFeed = [
        //     $tweet1,
        //     $tweet2,
        //     $tweet3
            // new Tweet($user, 'content...'),
            // new Tweet($user, 'content...'),
            // new Tweet($user, 'content...'),
            // [
            //     'tweetName' => '...'
            // ]
        // ];

        // data is passed into the view using an associative array
        // as the second argument to the 'view()' function.
        return view('welcome', [
            'user' => $user,
            'tweetFeed' => $tweetFeed
        ]);
    }

    public function getUser($userId) {

        $user = User::find($userId);
        $tweetFeed = Tweet::where('user_id', $userId)->get();

        return view('welcome', [
            'user' => $user,
            'tweetFeed' => $tweetFeed
        ]);
    }

    public function getTweetForm()
    {
        return view('tweetForm');
    }

    public function postTweetForm()
    {
        $request = request();

        $this->validate($request, [
            'content' => 'required|numeric'
        ]);

        $user = request()->user();

        if (!$user) {
            dd('You must be logged in!');
        }

        $tweet = new Tweet;
        $tweet->content = request()->input('content');
        $tweet->user_id = $user->id;
        $tweet->save();

        return redirect('/');
    }
}
