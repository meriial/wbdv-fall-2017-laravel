<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = factory(\App\Models\User::class)->create([]);

        factory(\App\Models\Tweet::class)->create([
            'user_id' => $user->id,
        ]);
    }
}
