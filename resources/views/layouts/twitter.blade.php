<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>
        <link rel="stylesheet" href="/css/app.css">
    </head>
    <body class="bg-color-1">
        @include('top-bar')
        @yield('content')
    </body>
</html>
