<form action="/tweet" method="post">
    <?php echo csrf_field() ?>

    <div class="<?php
        echo $errors->has('content') ? 'error' : ''
    ?>">
        <label for="content">Enter your tweet:</label>
        <textarea name="content" rows="8" cols="80"><?php echo old('content') ?></textarea>
        <?php if($errors->has('content')) { ?>
            <span>
                <?php echo $errors->first('content') ?>
            </span>
        <?php } ?>
    </div>

    <input type="submit" name="submit" value="Tweet!">
</form>
