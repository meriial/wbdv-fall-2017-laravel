<main class="flex-container">
    <div class="flex-1 left-column">
        <?php include '../resources/views/main-left.php' ?>
    </div>
    <div class="flex-2 padding-20">
        <?php
            if(Auth::check()) {
                include '../resources/views/tweetForm.php';
            }
        ?>
        <?php include '../resources/views/main-center.php' ?>
    </div>
    <div class="flex-1 padding-20">
        Right
    </div>
</main>
