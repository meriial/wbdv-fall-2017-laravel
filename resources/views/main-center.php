<div class="main-nav font-1 color-2">
    Tweets
</div>
<div class="tweets">
    <?php foreach ($tweetFeed as $tweet) { ?>
        <div class="tweet padding-10">
            <div class="flex-container font-5">
                <span class="tweet-name">
                    <a href="/user/<?php echo $tweet->user->id ?>">
                        <?php echo $tweet->user->name ?>
                    </a>
                </span>
                <span class="tweet-handle"><?php echo $tweet->user->handle ?></span>
                <span class="tweet-time padding-left-10"><?php echo $tweet->time ?></span>
            </div>
            <p class="font-3"><?php echo $tweet->content; ?></p>
            <div class="flex-container font-5">
                <span class="tweet-comments padding-right-10"><?php echo $tweet->comments() ?></span>
                <span class="tweet-retweets padding-right-10"><?php echo $tweet->retweets ?></span>
                <span class="tweet-likes"><?php echo $tweet->likes ?></span>
            </div>
        </div>
    <?php } ?>
</div>
