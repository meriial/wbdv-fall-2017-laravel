<div class="padding-10 bg-color-2">
    <a href="/">Home</a>
    <?php if(Auth::check()) { ?>
        <?php include '../resources/views/logout-button.php' ?>
    <?php } else { ?>
        <a href="/login">Login</a>
    <?php } ?>
</div>
