<div class="user-name"><?php echo $user->name ?></div>
<div class="user-handle"><?php echo $user->handle ?></div>
<div class="user-description">Dad, husband, President, citizen.</div>

<div class="user-location">Washington, DC</div>
<div class="user-website"><a href="#">obama.org</a></div>
<div class="user-joined">Joined March 2007</div>
<div class="user-birthday">Born on August 4, 1961</div>

<div class="user-images">
    <?php
        // be sure to close the php tags after the foreach
        // statement
     ?>
    <?php foreach ($user->images as $image) { ?>
        <img src="<?php echo $image ?>" alt="">
    <?php } ?>
</div>
