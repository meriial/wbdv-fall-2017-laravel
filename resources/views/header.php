<header>
    <div class="header-img">
        <img src="<?php echo $user->imgUrl ?>" alt="">
    </div>
    <div class="data-bar flex-container">
        <div class="data-counter-wrapper flex-1 flex-container">
            <div class="data-counter">
                <div class="data-counter-title">
                    Tweets
                </div>
                <div class="data-counter-value">
                    <?php echo $user->tweetCount ?>
                </div>
            </div>
            <div class="data-counter">
                <div class="data-counter-title">
                    Following
                </div>
                <div class="data-counter-value">
                    <?php echo $user->following ?>
                </div>
            </div>
            <div class="data-counter">
                <div class="data-counter-title">
                    Followers
                </div>
                <div class="data-counter-value">
                    <?php echo $user->followers ?>
                </div>
            </div>
            <div class="data-counter">
                <div class="data-counter-title">
                    Likes
                </div>
                <div class="data-counter-value">
                    <?php echo $user->likes ?>
                </div>
            </div>
            <div class="data-counter">
                <div class="data-counter-title">
                    Moments
                </div>
                <div class="data-counter-value">
                    <?php echo $user->moments ?>
                </div>
            </div>
        </div>

        <div class="button margin-right-40">
            Follow
        </div>
    </div>
</header>
