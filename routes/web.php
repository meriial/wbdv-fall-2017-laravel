<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// This will call the index 'action'
// on the WelcomeController
// when someone visits '/welcome' in their browser
Route::get('/', 'WelcomeController@index');

Route::get('/tweet', 'WelcomeController@getTweetForm');
Route::post('/tweet', 'WelcomeController@postTweetForm');

Route::get('/user/{userId}', 'WelcomeController@getUser');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
